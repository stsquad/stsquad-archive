* Standard Routine file by Matrixx of the ST Squad.

* Equates:

* MFP 68901 Registers.

IEA	EQU	$FFFFFA07	Interrupt enable A.
IEB	EQU	$FFFFFA09	Interrupt enable B.
ISRA	EQU	$FFFFFA0F	
IMA	EQU	$FFFFFA13	Interrupt mask A.
TBCR	EQU	$FFFFFA1B	Timer B control register.
TBDR	EQU	$FFFFFA21	Timer D control register.

* 68000 and MFP 68901 Exception Vectors.

VBLVec		EQU	$70	VBL vector.
KeyVec		EQU	$118	Keyboard interrupt vector.
HBLVec		EQU	$120	MFP HBL vector.

* Sound chip registers location.
SoundReg	EQU	$FFFF8800


* General MACROS and data storage.
DRed	MACRO
	move.w	#$700,$ffff8240.w
	ENDM
DGreen	MACRO
	move.w	#$070,$ffff8240.w
	ENDM
DBlue	MACRO
	move.w	#$007,$ffff8240.w
	ENDM


IntoSuper	MACRO
	LEA	Stack,A7
	CLR.L	-(SP)
	MOVE.W	#$20,-(SP)
	TRAP	#1
	ADDQ.L	#6,SP
	MOVE.L	D0,OldSSP
	ENDM

OutOfSuper	MACRO
	MOVE.L	OldSSP,-(SP)
	MOVE.W	#$20,-(SP)
	TRAP	#1
	ADDQ.L	#6,SP
	ENDM

LeaveProgram	MACRO
	CLR.W	-(SP)
	TRAP	#1
	ENDM

GetScreen	MACRO
	CLR.L	-(SP)		Get current screen position.
	MOVE.B	$FFFF8201.W,1(SP)
	MOVE.B	$FFFF8203.W,2(SP)
	MOVE.L	(SP)+,D0
	ENDM

SetScreen	MACRO
	MOVE.L	\1,-(SP)	Set the new screen position.
	MOVE.B	1(SP),$FFFF8201.W
	MOVE.B	2(SP),$FFFF8203.W
	ADDQ.L	#4,SP
	ENDM

KillMouse	MACRO
	MOVE.B	#$12,$FFFFFC02.W
	ENDM

ReviveMouse	MACRO
	MOVE.B	#$8,$FFFFFC02.W	Revive the mouse!
	ENDM

SwapScreens	MACRO
	SetScreen	ScreenPos	Set the new screen position.
	MOVE.L	ScreenPos2,D0	Swap the pointers.
	MOVE.L	ScreenPos,ScreenPos2
	MOVE.L	D0,ScreenPos
	WaitVBL
	ENDM

WaitVBL	MACRO
	CLR.W	VBLFlag
WaitVBLLoop\@
	TST.W	VBLFlag
	BEQ	WaitVBLLoop\@
	ENDM

SaveMFPStatus	MACRO
	LEA	MFPSave,A0
	LEA	(IEA).W,A1
	MOVEP.W	(A1),D0
	MOVE.W	D0,(A0)+
	MOVEP.W	12(A1),D0
	MOVE.W	D0,(A0)+
	MOVE.W	#$0100,D0
	MOVEP.W	D0,(A1)
	MOVEP.W	D0,12(A1)
	BCLR	#3,16(A1)
	ENDM
	

RestoreMFPStatus	MACRO
	LEA	MFPSave,A0
	LEA	(IEA).W,A1
	MOVE.W	(A0)+,D0
	MOVEP.W	D0,(A1)
	MOVE.W	(A0)+,D0
	MOVEP.W	D0,12(A1)
	BSET	#3,16(A1)
	ENDM

SavePallete	MACRO	
	MOVEQ	#7,D7
	LEA	$FFFF8240.W,A0
	LEA	OldPalette,A2
.SaveLoop
	MOVE.L	(A0)+,(A2)+
	DBRA	D7,.SaveLoop
	ENDM

RestorePallete	MACRO
	MOVEQ	#7,D7
	LEA	$FFFF8240.W,A0
	LEA	OldPalette,A2
.RestoreLoop
	MOVE.L	(A2)+,(A0)+
	DBRA	D7,.RestoreLoop
	ENDM

UnIce	MACRO
	lea	\1,a0
	lea	\2,a1
	jsr	decrunch
	ENDM

*********************
* General Routines. *
*********************

	JMP	Main

FlushKey:
	BTST	#0,$FFFFFC00.W
	BEQ.S	.Exit
	MOVE.B	$FFFFFC02.W,D0
	BRA.S	FlushKey
.Exit	RTS


* A0=Postion of the screen.
ClearScreen:
	MOVE.W	#1999,D7
	MOVEQ	#0,D0
.Loop	
	REPT	4
	MOVE.L	D0,(A0)+
	ENDR
	DBRA	D7,.Loop
	RTS

SaveCrash:
	lea	$00000008.w,a0
	lea	CrashSave,a1
	lea	Crash,a2
	move.w	#5,d0
.loop
	move.l	(a0),(a1)+
	move.l	a2,(a0)+
	dbra	d0,.loop

	rts

RestoreCrash:
	lea	$00000008.w,a0
	lea	CrashSave,a1
	move.w	#5,d0
.loop
	move.l	(a1)+,(a0)+
	dbra	d0,.loop

	rts

Crash:
	movem.l	d0-d7/a0-a6,-(sp)
	bsr	RestoreCrash
	bsr	FlushKey
	RestoreMFPStatus
	ReviveMouse
	SetScreen	TrueScreenPosition
	RestorePallete
	movem.l	(sp)+,d0-d7/a0-a6
	rte

;***************************************************************************
; Unpacking source for Pack-Ice Version 2.1
; a0: Pointer on packed Data
; a1: Pointer on destination of unpacked data
decrunch:	movem.l d0-a6,-(sp)
		cmpi.l	#'Ice!',(a0)+	; Is data packed?
		bne.s	ice_03		; no!
		move.l	(a0)+,d0	; read packed data
		lea	-8(a0,d0.l),a5
		move.l	(a0)+,(sp)
		movea.l a1,a4
		movea.l a1,a6
		adda.l	(sp),a6
		movea.l a6,a3
		bsr.s	ice_08
		bsr.s	ice_04
		bsr	ice_0c		;; Picture decrunch!
		bcc.s	ice_03		;; These marked lines may be
		move.w	#$0f9f,d7	;; removed in your own sources
ice_00:		moveq	#3,d6		;; if you do not use the
ice_01:		move.w	-(a3),d4	;; additional algorithm.
		moveq	#3,d5		;;
ice_02:		add.w	d4,d4		;;
		addx.w	d0,d0		;;
		add.w	d4,d4		;;
		addx.w	d1,d1		;;
		add.w	d4,d4		;;
		addx.w	d2,d2		;;
		add.w	d4,d4		;;
		addx.w	d3,d3		;;
		dbra	d5,ice_02	;;
		dbra	d6,ice_01	;;
		movem.w d0-d3,(a3)	;;
		dbra	d7,ice_00	;;
ice_03:		movem.l (sp)+,d0-a6
		rts
ice_04:		bsr.s	ice_0c
		bcc.s	ice_07
		moveq	#0,d1
		bsr.s	ice_0c
		bcc.s	ice_06
		lea	ice_1a(pc),a1
		moveq	#4,d3
ice_05:		move.l	-(a1),d0
		bsr.s	ice_0f
		swap	d0
		cmp.w	d0,d1
		dbne	d3,ice_05
		add.l	20(a1),d1
ice_06:		move.b	-(a5),-(a6)
		dbra	d1,ice_06
ice_07:		cmpa.l	a4,a6
		bgt.s	ice_12
		rts
ice_08:		moveq	#3,d0
ice_09:		move.b	-(a5),d7
		ror.l	#8,d7
		dbra	d0,ice_09
		rts
ice_0a:		move.w	a5,d7
		btst	#0,d7
		bne.s	ice_0b
		move.l	-(a5),d7
		addx.l	d7,d7
		bra.s	ice_11
ice_0b:		move.l	-5(a5),d7
		lsl.l	#8,d7
		move.b	-(a5),d7
		subq.l	#3,a5
		add.l	d7,d7
		bset	#0,d7
		bra.s	ice_11
ice_0c:		add.l	d7,d7
		beq.s	ice_0d
		rts
ice_0d:		move.w	a5,d7
		btst	#0,d7
		bne.s	ice_0e
		move.l	-(a5),d7
		addx.l	d7,d7
		rts
ice_0e:		move.l	-5(a5),d7
		lsl.l	#8,d7
		move.b	-(a5),d7
		subq.l	#3,a5
		add.l	d7,d7
		bset	#0,d7
		rts
ice_0f:		moveq	#0,d1
ice_10:		add.l	d7,d7
		beq.s	ice_0a
ice_11:		addx.w	d1,d1
		dbra	d0,ice_10
		rts
ice_12:		lea	ice_1b(pc),a1
		moveq	#3,d2
ice_13:		bsr.s	ice_0c
		dbcc	d2,ice_13
		moveq	#0,d4
		moveq	#0,d1
		move.b	1(a1,d2.w),d0
		ext.w	d0
		bmi.s	ice_14
		bsr.s	ice_0f
ice_14:		move.b	6(a1,d2.w),d4
		add.w	d1,d4
		beq.s	ice_16
		lea	ice_1c(pc),a1
		moveq	#1,d2
ice_15:		bsr.s	ice_0c
		dbcc	d2,ice_15
		moveq	#0,d1
		move.b	1(a1,d2.w),d0
		ext.w	d0
		bsr.s	ice_0f
		add.w	d2,d2
		add.w	6(a1,d2.w),d1
		bra.s	ice_18
ice_16:		moveq	#0,d1
		moveq	#5,d0
		moveq	#0,d2
		bsr.s	ice_0c
		bcc.s	ice_17
		moveq	#8,d0
		moveq	#$40,d2
ice_17:		bsr.s	ice_0f
		add.w	d2,d1
ice_18:		lea	2(a6,d4.w),a1
		adda.w	d1,a1
		move.b	-(a1),-(a6)
ice_19:		move.b	-(a1),-(a6)
		dbra	d4,ice_19
		bra	ice_04
		DC.B $7f,$ff,$00,$0e,$00,$ff,$00,$07
		DC.B $00,$07,$00,$02,$00,$03,$00,$01
		DC.B $00,$03,$00,$01
ice_1a:		DC.B $00,$00,$01,$0d,$00,$00,$00,$0e
		DC.B $00,$00,$00,$07,$00,$00,$00,$04
		DC.B $00,$00,$00,$01
ice_1b:		DC.B $09,$01,$00,$ff,$ff,$08,$04,$02
		DC.B $01,$00
ice_1c:		DC.B $0b,$04,$07,$00,$01,$20,$00,$00
		DC.B $00,$20
;***************************************************************************



SECTION BSS

	DS.L	200
Stack	DS.W	1

OldSSP		DS.L	1
VBLFlag		DS.W	1

MFPSave	DS.W	2

CrashSave
	ds.l	6
OldPalette
	ds.w	16

SECTION TEXT