	OPT	O+,OW-,C+
* Standard null demo, written by Matrixx of the ST Squad 1990.
* With the Stampede Demo by Archie
* Converted from rAMpage disk magazine on the Amiga
* Copyright 1990 ST Squad.

	INCLUDE	C:\STAMPEDE.INC\MACRO.S

*****Gen Data
MODData:				*the module
	incbin 	C:\STAMPEDE.INC\MASQURAD.MOD
	even
TrackDat:
	incbin	C:\STAMPEDE.INC\TRACKER.ROT
	even
ScreenDat:
	incbin	C:\STAMPEDE.INC\BACK.SPR
	even
*****

******************
*Service Routines*
******************
SetUpMusic:	
	clr.b	$fffffa09.w		*init interrupts
	clr.b	$fffffa15.w
	move.w	Module+$90,d0
	lea	mfp_freqs,a0
	move.b	(a0,d0.w),$fffffa1f.w
	move.b	#2,$fffffa19.w
	move.b	#32,$fffffa07.w
	move.b	#32,$fffffa13.w
	bclr	#3,$fffffa17.w
	move.l	#Player+$8918,$134
	rts
mfp_freqs:
	dc.b	24			*10 and 8.3 Khz
	dc.b	29
MakeScreen:
	lea	ScreenDat,a0
	move.l	ScreenPos,a1
	sub.l	#16000,a1
	jsr	decrunch

	lea	ScreenDat,a0
	move.l	ScreenPos2,a1
	sub.l	#16000,a1
	jsr	decrunch

	rts


ShiftSprites:
	lea	SpriteData,a0
	lea	LetterS,a1

	moveq.l	#15-1,d2
.Letters
	moveq.l	#7,d1
.Loop
	moveq.l	#0,d0
	move.w	(a0)+,d0
	swap	d0
	move.l	d0,(a1)+

	dbra	d1,.Loop
	lea	512-32(a1),a1
	dbra	d2,.Letters

	lea	LetterS,a0
	move.l	a0,a2
	moveq.l	#15-1,d3
.Chars
	lea	32(a0),a1
	moveq.l	#14,d1
.Number
	moveq.l	#7,d2
.Lines	
	move.l	(a0)+,d0
	lsr.l	#1,d0
	move.l	d0,(a1)+
	dbra	d2,.Lines

	lea	-32(a1),a0

	dbra	d1,.Number

	lea	512(a2),a2
	move.l	a2,a0
	dbra	d3,.Chars

	rts

SpriteData:
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1100000000000000
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%0000000000000011
	dc.w	%1111111111111111
	dc.w	%1111111111111111

	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%0000000110000000
	dc.w	%0000000110000000
	dc.w	%0000000110000000
	dc.w	%0000000110000000
	dc.w	%0000000110000000
	dc.w	%0000000110000000

	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1100000000000011
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1100000000000011
	dc.w	%1100000000000011
	dc.w	%1100000000000011
	
	dc.w	%1100000000000011
	dc.w	%1110000000000111
	dc.w	%1111000000001111
	dc.w	%1101100000011011
	dc.w	%1100110000110011
	dc.w	%1100001111000011
	dc.w	%1100000110000011
	dc.w	%1100000000000011

	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1100000000000011
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1100000000000000
	dc.w	%1100000000000000
	dc.w	%1100000000000000

	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1100000000000000
	dc.w	%1111111111110000
	dc.w	%1111111111110000
	dc.w	%1100000000000000
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	
	dc.w	%1111111111110000
	dc.w	%1111111111111100
	dc.w	%1100000000000110
	dc.w	%1100000000000011
	dc.w	%1100000000000011
	dc.w	%1100000000000110
	dc.w	%1111111111111100
	dc.w	%1111111111110000

	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%0000000111000000
	dc.w	%0000000111000000
	dc.w	%0000000111000000
	dc.w	%0000000111000000
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1100000000000000
	dc.w	%1100000000000000
	dc.w	%1100000000000000
	dc.w	%1100000000000000
	dc.w	%1111111111111111
	dc.w	%1111111111111111	

	dc.w	%1111111111110000
	dc.w	%1111111111110000
	dc.w	%1100000000111000
	dc.w	%1111111111110000
	dc.w	%1111111111111110
	dc.w	%1100000000000111
	dc.w	%1111111111111110
	dc.w	%1111111111111110

	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1100000000000000
	dc.w	%1100000000111110
	dc.w	%1100000000111110
	dc.w	%1100000000001100
	dc.w	%1111111111111100
	dc.w	%1111111111111100

	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%0000000001110000
	dc.w	%0000000111000000
	dc.w	%0000001110000000
	dc.w	%0000111000000000
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	
	dc.w	%1110000000000011
	dc.w	%1111100000000011
	dc.w	%1101111000000011
	dc.w	%1100011110000011
	dc.w	%1100000111100011
	dc.w	%1100000001111011
	dc.w	%1100000000011111
	dc.w	%1100000000000111

	dc.w	%1111000000000000
	dc.w	%1111111100000000
	dc.w	%1111111111110000
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%1111111111110000	
	dc.w	%1111111100000000
	dc.w	%1111000000000000


	dc.w	%0000000000001111
	dc.w	%0000000011111111
	dc.w	%0000111111111111
	dc.w	%1111111111111111
	dc.w	%1111111111111111
	dc.w	%0000111111111111
	dc.w	%0000000011111111
	dc.w	%0000000000001111


	even

SetDelTab:
	lea	Table1,a0
	move.l	ScreenPos,a1
	lea	6(a1),a1
	move.l	a1,d0
	lea	Table2,a1
	moveq.l	#0,d1
	move.w	NumberS,d1
.loop
	move.l	d0,(a0)+
	move.l	d0,(a1)+
	dbra	d1,.loop

	rts

* Interrupt handlers:
VBLHandler:
	MOVE.W	#$FFFF,VBLFlag
	MOVEM.L d0-d5/a0-a1,-(SP)

	lea	$ffff8240.w,a0
	moveq.l	#0,d0
	rept	8
	move.l	d0,(a0)+
	endr
	
	LEA 	SyncTable,A0	;SETUP CORRECT LINE JUMPS
	moveq.l	#0,d0
	moveq.l	#0,d1	

	MOVE.B 	ScreenPos2+3,D0
	LSR.W 	#3,D0
	
	LSL.W	#2,D0	=*36(9*4)
	MOVE.W	D0,D1
	LSL.W	#3,D0
	ADD.W	D1,D0

	ADDA.W 	D0,A0

	MOVE.L 	(A0)+,AlterMe1+2
	MOVE.L 	(A0)+,AlterMe2+2
	MOVE.L 	(A0)+,AlterMe3+2
	MOVE.L 	(A0)+,AlterMe4+2
	MOVE.L 	(A0)+,AlterMe5+2
	MOVE.L 	(A0)+,AlterMe6+2
	MOVE.L 	(A0)+,AlterMe7+2
	MOVE.L 	(A0)+,AlterMe8+2
	MOVE.L 	(A0)+,AlterMe9+2

	move.l	#1785,d0
.loop
	dbra	d0,.loop

*	move.w	#$777,$ffff8240.w
	
*	bra	NoScroll
* EXTRA ROUTINES CAN FIT IN HERE


	MOVE 	#$2700,SR		;SWITCH OFF INTERRUPTS
	move.w	#$4e75,Player+$894e
	
	lea 	$ffff8209.w,a0	Hold on
HangOn:	
	move.b 	(A0),D0
	beq.s	HangOn
	and.w 	#$3F,D0
	MOVEQ 	#$3F,D1
	SUB 	D0,D1
	LSL 	D1,D0
	DCB.W 	39+2,$4E71
*that 39+2(lea)-5(move.w)-0(fiddle)
*	move.w	#$000,$ffff8240.w
	LEA 	$FFFF8260.W,A0
	LEA 	$FFFF820A.W,A1
*	LEA 	$FFFF8209.W,A2
	MOVEQ	#0,D0
	MOVEQ	#2,D1
AlterMe1:
	JSR 0
AlterMe2:
	JSR 0
AlterMe3:
	JSR 0
AlterMe4:
	JSR 0
AlterMe5:
	JSR 0
AlterMe6:
	JSR 0
AlterMe7:
	JSR 0
AlterMe8:
	JSR 0
AlterMe9:
	JSR 0
	JSR RSW
NoScroll:
	move.w	#$4e73,Player+$894e
*	move.w	#$2300,sr

	lea	Palette,a0
	lea	$ffff8240.w,a1
	rept	8
	move.l	(a0)+,(a1)+
	endr

	move.w	#$2300,sr
	
	addq.w	#1,NoFrames

	CLR.B	(TBCR).W
	MOVE.B	#187,(TBDR).W
	MOVE.B	#8,(TBCR).W
	MOVE.B	#187,(TBDR).W

	
	MOVEM.L (A7)+,D0-D5/A0-A1
	RTE
		
RSM:
	DCB.W 109-64,$4E71
	jsr	Player+$8918
	MOVE.B D0,(A1)
	DCB.W 4,$4E71
	MOVE.B D1,(A1)
	DCB.W 2,$4E71
	RTS
RSW:
	DCB.W 109,$4E71
	MOVE.B D0,(A1)
	DCB.W 4,$4E71
	MOVE.B D1,(A1)
	DCB.W 2,$4E71
	RTS
DNW:
	DCB.W 	119,$4E71
*	jsr	Player+$8918
	RTS
DNM:
	DCB.W 	119-64,$4E71
	jsr	Player+$8918
	RTS
R2M:	
	NOP
	MOVE.B 	D1,(A0)
	NOP
	MOVE.B 	D0,(A0)
	DCB.W 	14,$4E71
	MOVE.B 	D1,(A0)
	MOVE.B 	D0,(A0)
	DCB.W 	85-64,$4E71
	jsr	Player+$8918
	MOVE.B 	D0,(A1)
	DCB.W 	4,$4E71
	MOVE.B 	D1,(A1)
	DCB.W 	2,$4E71
	RTS
R2W:	
	NOP
	MOVE.B 	D1,(A0)
	NOP
	MOVE.B 	D0,(A0)
	DCB.W 	14,$4E71
	MOVE.B 	D1,(A0)
	MOVE.B 	D0,(A0)
	DCB.W 	85,$4E71
	MOVE.B 	D0,(A1)
	DCB.W 	4,$4E71
	MOVE.B 	D1,(A1)
	DCB.W 	2,$4E71
	RTS
R3W:
	NOP
	MOVE.B D1,(A0)
	NOP
	MOVE.B 	D0,(A0)
	DCB.W 	14,$4E71
	MOVE.B 	D1,(A0)
	MOVE.B 	D0,(A0)
	DCB.W 	95,$4E71
	RTS
R3M:
	NOP
	MOVE.B D1,(A0)
	NOP
	MOVE.B 	D0,(A0)
	DCB.W 	14,$4E71
	MOVE.B 	D1,(A0)
	MOVE.B 	D0,(A0)
	DCB.W 	95-64,$4E71
	jsr	Player+$8918
	RTS
R4M:
	DCB.W 	113-64,$4E71
	jsr	Player+$8918
	MOVE.B 	D0,(A1)
	MOVE.B 	D1,(A1)
	DCB.W 	2,$4E71
	RTS
R4W:
	DCB.W 	113,$4E71
	MOVE.B 	D0,(A1)
	MOVE.B 	D1,(A1)
	DCB.W 	2,$4E71
	RTS
R5W:
	NOP
	MOVE.B 	D1,(A0)
	NOP
	MOVE.B 	D0,(A0)
	DCB.W 	14,$4E71
	MOVE.B 	D1,(A0)
	MOVE.B 	D0,(A0)
	DCB.W 	89,$4E71
*	jsr	Player+$8918
	MOVE.B 	D0,(A1)
	MOVE.B 	D1,(A1)
	DCB.W 	2,$4E71
	RTS
R5M:
	NOP
	MOVE.B 	D1,(A0)
	NOP
	MOVE.B 	D0,(A0)
	DCB.W 	14,$4E71
	MOVE.B 	D1,(A0)
	MOVE.B 	D0,(A0)
	DCB.W 	89-64,$4E71
	jsr	Player+$8918
	MOVE.B 	D0,(A1)
	MOVE.B 	D1,(A1)
	DCB.W 	2,$4E71
	RTS


Palette:
	DC.W	$000
	DC.W	$001
	DC.W	$002
	DC.W	$003
	DC.W	$333
	DC.W	$222
	DC.W	$555
	DC.W	$777	
	DCB.W	8,$700

SyncTable
ScrollTab:
	dc.l	R5M,R5W,R3M,DNW,DNM,DNW,RSM,RSW,DNM
	dc.l	R5M,R3W,R3M,R3W,R3M,DNW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R4M,DNW,DNM,RSW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R3M,R3W,DNM,RSW,RSM,RSW,DNM 
	dc.l	R5M,R4W,R4M,R4W,RSM,RSW,RSM,RSW,DNM
	dc.l	R5M,R5W,R5M,DNW,RSM,RSW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R5M,DNW,DNM,DNW,DNM,DNW,DNM 
	dc.l	R5M,R5W,R3M,R3W,R3M,DNW,DNM,DNW,DNM 
	dc.l	R5M,R5W,R5M,R2W,RSM,RSW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R3W,DNM,DNW,DNM,RSW,DNM 
	dc.l	R5M,R5W,R3M,R3W,R3M,R3W,DNM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R4W,DNM,DNW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R3W,R3M,DNW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R4M,R4W,R4M,RSW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R5W,DNM,RSW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R3W,R3M,R2W,RSM,RSW,DNM 
	dc.l	R4M,R4W,R4M,R4W,R4M,R4W,R3M,DNW,DNM
	dc.l	R5M,R5W,R5M,R5W,R2M,RSW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R5W,R3M,DNW,DNM,DNW,DNM 
	dc.l	R5M,R5W,R5M,R3W,R3M,R3W,R3M,DNW,DNM 
	dc.l	R5M,R5W,R5M,R5W,R4M,DNW,DNM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R5W,R3M,R3W,DNM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R4W,R4M,R4W,RSM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R5W,R5M,DNW,RSM,RSW,DNM 
	dc.l	R5M,R5W,R5M,R5W,R2M,R3W,R2M,DNW,DNM 
	dc.l	R5M,R5W,R5M,R3W,R3M,R3W,R3M,R2W,R2M
	dc.l	R5M,R5W,R5M,R4W,R4M,R2W,R2M,R2W,DNM
	dc.l	R5M,R4W,R4M,R4W,R4M,R4W,R4M,R4W,DNM
	dc.l	R5M,R5W,R4M,R4W,R4M,R4W,R4M,R3W,DNM
	dc.l	R5M,R5W,R5M,R4W,R4M,R4W,R3M,R3W,DNM
	dc.l	R5M,R5W,R5M,R5W,R5M,R3W,R3M,DNW,DNM
	dc.l	R5M,R5W,R5M,R5W,R4M,R4W,R4M,RSW,DNM
	
NoFrames:
	dc.w	0

HBLHandler:
	move.w	#$2700,sr

	move.l	a0,-(sp)
	move.l	d0,-(sp)

	lea	$fffffa21.w,a0	; timer B data

	moveq.l	#0,d0
	move.b 	(a0),d0
.Hang
	cmp.b 	(a0),d0		auf ende der letzten zeile warten
	beq.s 	.Hang
*				wir sind rechts im rand
	clr.b 	$ffff820a	60Hz

	rept	15
	nop	
	endr

	move.b	#2,$ffff820a.w
	
	move.l	(sp)+,d0
	move.l	(sp)+,a0	

	rte

*****************
* Main Program. *
*****************

Main:
	IntoSuper
	GetScreen
	MOVE.L	D0,TrueScreenPosition
	
	MOVEQ	#7,D7
	LEA	$FFFF8240.W,A0
	LEA	Palette,A1
	LEA	OldPalette,A2
SetPaletteLoop:
	MOVE.L	(A0),(A2)+
	MOVE.L	(A1)+,(A0)+
	DBRA	D7,SetPaletteLoop

	JSR	FlushKey
	SetScreen	ScreenPos
	GetScreen	d0
	move.l	d0,ScreenPos
	SetScreen	ScreenPos2
	GetScreen	d0
	move.l	d0,ScreenPos2
	
*******Un-Pack Files******

	UnIce	MODData,Module
	UnIce	TrackDat,Player
**************************

*Setup Graphics here
	jsr	MakeScreen

	jsr	ShiftSprites

	jsr	SetDelTab
*
	MOVE.W	#$2700,SR
	MOVE.L	(VBLVec).W,OldVBLVec
	MOVE.L	#VBLHandler,(VBLVec).W
	KillMouse
	SaveMFPStatus
	MOVE.L	(HBLVec).W,OldHBLVec
	MOVE.L	#HBLHandler,(HBLVec).W

	move.l	$134.w,OldTimerA
*	move.l	#Player+$8918,$134.w
SetUpPlayer:
	* you can use all registers here

	lea	Module,a0		*a0=module start
	moveq	#0,d0			*-1=the default soundtable 0=the other
	jsr	Player+$24		*init. replay routine
	
	* don't use d6-d7/a2-a6 from here

	jsr	SetUpMusic		*init. interrupts

	move.b	#0,$fffffa1b.w
	or.b	#1,$fffffa07.w
	or.b	#1,$fffffa13.w


	MOVE.W	#$2300,SR
	
MainLoop:
	move.w	#$000,$ffff8240.w
	SwapScreens
	jsr	ScrollBit
*	move.w	#$777,$ffff8240.w
	jsr	DelStuff
*	move.w	#$700,$ffff8240.w
	jsr	PlotStuff
*	move.w	#$274,$ffff8240.w
	jsr	Player+$28		*call replay routine
*	move.w	#$004,$ffff8240.w
	jsr	VUDisplay
	move.w	#$007,$ffff8240.w

************************************************************************
* Replay routine that replays -TCB TRACKER- modules  in normal ST mode *
* Made by Anders Nilsson  10/8/90                                      *
* Uses d6-d7/a2-a6 and usp                                             *
************************************************************************

	BTST.B	#0,$FFFFFC00.W
	BEQ	MainLoop
	MOVE.B	$FFFFFC02.W,D0
	CMPI.B	#185,D0
	BEQ	Exit
	cmpi.b	#205,d0
	beq	MoreX
	cmpi.b	#203,d0
	beq	LessX
	cmpi.b	#200,d0
	beq	MoreY
	cmpi.b	#208,d0
	beq	LessY
	cmpi.b	#232,d0
	beq	FasterX
	cmpi.b	#238,d0
	beq	SlowerX
	cmpi.b	#236,d0
	beq	FasterY
	cmpi.b	#234,d0
	beq	SlowerY
	
	bra	MainLoop

MoreX:
	cmp.w	#$18,XInc
	beq	MainLoop
	add.w	#$4,XInc
	bra	MainLoop
LessX:
	tst.w	XInc
	beq	MainLoop
	sub.w	#$4,XInc
	bra	MainLoop

MoreY:
	cmp.w	#$e,YInc
	beq	MainLoop
	add.w	#$2,YInc
	bra	MainLoop
LessY:
	tst.w	YInc
	beq	MainLoop
	sub.w	#$2,YInc
	bra	MainLoop
FasterX:
	cmp.w	#$18,XSpeed
	beq	MainLoop
	add.w	#$4,XSpeed
	bra	MainLoop
SlowerX:
	tst.w	XSpeed
	beq	MainLoop
	sub.w	#$4,XSpeed
	bra	MainLoop
FasterY:
	cmp.w	#$e,YSpeed
	beq	MainLoop
	add.w	#$2,YSpeed
	bra	MainLoop
SlowerY:
	tst.w	YSpeed
	beq	MainLoop
	sub.w	#$2,YSpeed
	bra	MainLoop

Exit:
	move.w	#$2700,sr

	JSR	FlushKey

	LEA	OldPalette,A1
	LEA	$FFFF8240.W,A0
	MOVEQ	#7,D7
RestorePaletteLoop:
	MOVE.L	(A1)+,(A0)+
	DBRA	D7,RestorePaletteLoop
	
	MOVE.W	#$2700,SR
	MOVE.L	OldVBLVec,(VBLVec).W
	MOVE.L	OldHBLVec,(HBLVec).W
	move.l	OldTimerA,$134.w	
	RestoreMFPStatus
	ReviveMouse
	MOVE.W	#$2300,SR
	
	SetScreen	TrueScreenPosition
	OutOfSuper
	LeaveProgram

*This bit does the scrolling

ScrollBit:

SortYScroll:
	move.l	ScrollYPos,a0
	move.l	(a0)+,d0
	add.l	d0,ScreenPos
	add.l	d0,ScreenPos2
	lea	EndYScroll,a1
	cmp.l	a1,a0
	bne.s	.Ok
*	move.w	#$777,$ffff8240.w
	move.l	#StartYScroll,a0
.Ok
	move.l	a0,ScrollYPos
	bra	SortXScroll

ScrollYPos:
	dc.l	StartYScroll
StartYScroll:
	dcb.l	50,0
	INCBIN	C:\STAMPEDE.INC\HARD.WAV
	dcb.l	6,-160
EndYScroll:
	dc.l	0

SortXScroll:
	tst.l	ScrollXCount
	beq.s	NextVal
ComeBack:
	move.l	ScrollXVal,d0
	add.l	d0,ScreenPos
	add.l	d0,ScreenPos2
	subq.l	#1,ScrollXCount
	rts
NextVal:
	move.l	ScrollXPos,a0
	move.l	(a0)+,ScrollXCount
	move.l	(a0)+,ScrollXVal
	lea	EndXScroll,a1
	cmp.l	a1,a0
	bne.s	.Ok
*	move.w	#$777,$ffff8240.w
	move.l	#StartXScroll,a0
.Ok
	move.l	a0,ScrollXPos
	bra.s	ComeBack
ScrollXCount:
	dc.l	1150
ScrollXVal:
	dc.l	0
ScrollXPos:
	dc.l	StartXScroll-16
	dc.l	50,16
	dc.l	100,-8
StartXScroll:
	dc.l	200,-8
	dc.l	200,0
	dc.l	200,8
	dc.l	200,0
	dc.l	10,-8
	dc.l	10,8
	dc.l	20,0
	dc.l	20,8
	dc.l	50,0
	dc.l	40,-8
	dc.l	100,0
EndXScroll:
	dc.l	0

*Just Testing
DelStuff:

*	MOVE.W	#$500,$FFFF8240.W

	moveq.l	#0,d5
	move.l	d5,d0
	move.w	NumberS(pc),d5
	move.l	LastPos,a1

.loop
	move.l	(a1)+,a0
offset	set	0
	rept	8
	move.w	d0,offset(a0)
	move.w	d0,8+offset(a0)
offset	set	offset+160
	endr

	dbra	d5,.loop

	move.l	LastPos(pc),d0
	move.l	CurrentPos(pc),LastPos
	move.l	d0,CurrentPos

	jmp	DelVU
	
LastPos:
	dc.l	Table1
CurrentPos:
	dc.l	Table2	

Table1:	
	rept	30
	dc.l	ScreenOne
	endr
Table2:
	rept	30
	dc.l	ScreenOne
	endr


PlotStuff:

	move.l	SpriteYPos(pc),a0
	add.w	YSpeed(pc),a0
	lea	EndSpriteY(pc),a1
	cmp.l	a1,a0
	BMI.S	.Ok
	SUB.L	#EndSpriteY-SpriteYTable,a0
.Ok
	move.l	a0,SpriteYPos

	move.l	SpriteXPos(pc),a0
	add.w	XSpeed,a0
	lea	EndSpriteX(pc),a1
	cmp.l	a1,a0
	BMI.S	.Ok2
	SUB.L	#EndSpriteX-SpriteXTable,a0
.Ok2
	move.l	a0,SpriteXPos
	

	moveq.l	#0,d5
	move.w	NumberS(pc),d5
	
	moveq.l	#0,d3
	move.l	d3,d2

	move.w	YInc(PC),.Alter+2

	move.w	XInc(PC),.Alter2+2

	move.l	d5,d4

	add.l	d4,d4
	add.l	d4,d4

.loop

	lea	SpriteInfo(pc),a1
	move.l	(a1,d4.w),d1

	move.l	SpriteYPos(pc),a1
	move.w	(a1,D3),d0
.Alter	add.w	#$4,D3

	move.l	SpriteXPos(pc),a1
	add.w	(a1,D2),d0
	add.w	2(a1,D2),d1
.Alter2	add.w	#0,D2

	move.l	ScreenPos(PC),a0
	lea	6(a0,d0.w),a0
	move.l	CurrentPos(pc),a1
	move.l	a0,(a1,d4.w)
	lea	.SpriteRouts(pc),a1
	move.l	(a1,d4.w),a1

	JMP	(a1)
.Return	

	SUBQ.W	#4,D4

	dbra	d5,.loop

*	MOVE.W	#$70,$FFFF8240.W

	rts

.StandardLetter:
	move.l	d1,a1
offset	set	0
	rept	8
	move.l	(a1)+,d0
	or.w	d0,8+offset(a0)
	swap	d0
	or.w	d0,offset(a0)
offset	set	offset+160
	endr

	BRA.S	.Return

.LetterTRout:

	move.l	d1,a1
	move.l	(a1)+,d0
	or.w	d0,8(a0)
	or.w	d0,8+160(a0)
	swap	d0
	or.w	d0,(a0)
	or.w	d0,160(a0)

	addq.l	#4,a1
	move.l	(a1)+,d0
	or.w	d0,8+320(a0)
	or.w	d0,8+480(a0)
	or.w	d0,8+640(a0)
	or.w	d0,8+800(a0)
	or.w	d0,8+960(a0)
	or.w	d0,8+1120(a0)
	swap	d0
	or.w	d0,320(a0)
	or.w	d0,480(a0)
	or.w	d0,640(a0)
	or.w	d0,800(a0)
	or.w	d0,960(a0)
	or.w	d0,1120(a0)

	BRA	.Return

.LetterARout:
	move.l	d1,a1
	move.l	(a1)+,d0
	or.w	d0,8(a0)
	or.w	d0,8+160(a0)
	or.w	d0,8+480(a0)
	or.w	d0,8+640(a0)
	swap	d0
	or.w	d0,(a0)
	or.w	d0,160(a0)
	or.w	d0,480(a0)
	or.w	d0,640(a0)

	addq.l	#4,a1
	move.l	(a1)+,d0
	or.w	d0,8+(2*160)(a0)
	or.w	d0,8+(5*160)(a0)
	or.w	d0,8+(6*160)(a0)
	or.w	d0,8+(7*160)(a0)
	swap	d0
	or.w	d0,(2*160)(a0)
	or.w	d0,(5*160)(a0)
	or.w	d0,(6*160)(a0)
	or.w	d0,(7*160)(a0)

	BRA	.Return

.LetterPRout:
	move.l	d1,a1
	move.l	(a1)+,d0
	or.w	d0,8(a0)
	or.w	d0,648(a0)
	swap	d0
	or.w	d0,(a0)
	or.w	d0,640(a0)
	move.l	(a1)+,d0
	or.w	d0,168(a0)
	or.w	d0,488(a0)
	swap	d0
	or.w	d0,160(a0)
	or.w	d0,480(a0)
	move.l	(a1)+,d0
	or.w	d0,328(a0)
	swap	d0
	or.w	d0,320(a0)
	move.l	8(a1),d0
	or.w	d0,808(a0)
	or.w	d0,968(a0)
	or.w	d0,1128(a0)
	swap	d0
	or.w	d0,800(a0)
	or.w	d0,960(a0)
	or.w	d0,1120(a0)
	BRA	.Return

.LetterERout:
	move.l	d1,a1
	move.l	(a1)+,d0
	or.w	d0,8(a0)
	or.w	d0,1128(a0)
	swap	d0
	or.w	d0,(a0)
	or.w	d0,1120(a0)
	move.l	(a1)+,d0
	or.w	d0,168(a0)
	or.w	d0,328(a0)
	or.w	d0,648(a0)
	or.w	d0,808(a0)
	or.w	d0,968(a0)
	swap	d0
	or.w	d0,160(a0)
	or.w	d0,320(a0)
	or.w	d0,640(a0)
	or.w	d0,800(a0)
	or.w	d0,960(a0)
	move.l	4(a1),d0
	or.w	d0,488(a0)
	swap	d0
	or.w	d0,480(a0)
	BRA	.Return

.LetterIRout:
	move.l	d1,a1
	move.l	(a1)+,d0
	or.w	d0,8(a0)
	or.w	d0,1128(a0)
	swap	d0
	or.w	d0,(a0)
	or.w	d0,1120(a0)

	move.l	(a1)+,d0
	or.w	d0,8+160(a0)
	or.w	d0,8+320(a0)
	or.w	d0,8+480(a0)
	or.w	d0,8+640(a0)
	or.w	d0,8+800(a0)
	or.w	d0,8+960(a0)
	swap	d0
	or.w	d0,(a0)
	or.w	d0,160(a0)
	or.w	d0,320(a0)
	or.w	d0,480(a0)
	or.w	d0,640(a0)
	or.w	d0,800(a0)
	or.w	d0,960(a0)

	BRA	.Return

.LetterDRout:
	move.l	d1,a1
offset	set	0
	rept	4
	move.l	(a1)+,d0
	or.w	d0,8+offset(a0)
	or.w	d0,8+(1120-offset)(a0)
	swap	d0
	or.w	d0,offset(a0)
	or.w	d0,(1120-offset)(a0)
offset	set	offset+160
	endr

	BRA	.Return
	

	
.NullLetter:

	BRA	.Return

.SpriteRouts:
	dc.l	.StandardLetter
	dc.l	.StandardLetter
	dc.l	.LetterTRout
	dc.l	.LetterARout
	dc.l	.StandardLetter
	dc.l	.StandardLetter
	dc.l	.StandardLetter
	dc.l	.StandardLetter
	dc.l	.StandardLetter
*	dc.l	.LetterPRout
*	dc.l	.LetterERout
*	dc.l	.LetterDRout
*	dc.l	.LetterERout
	dc.l	.NullLetter
	dc.l	.StandardLetter
	dc.l	.StandardLetter
*	dc.l	.LetterDRout
*	dc.l	.LetterIRout
	dc.l	.StandardLetter
	dc.l	.StandardLetter
	dc.l	.StandardLetter
	dc.l	.LetterARout
	dc.l	.StandardLetter
	dc.l	.StandardLetter
	dc.l	.StandardLetter
*	dc.l	.LetterERout
*	dc.l	.LetterDRout
	dc.l	.NullLetter
	dc.l	.StandardLetter
	dc.l	.LetterARout
	dc.l	.StandardLetter
	dc.l	.LetterARout
	dc.l	.StandardLetter
	dc.l	.StandardLetter
*	dc.l	.LetterIRout
	dc.l	.StandardLetter
	dc.l	.StandardLetter
*	dc.l	.LetterERout
	dc.l	.StandardLetter
	dc.l	.NullLetter


NumberS:
	dc.w	28
XInc:	
	dc.w	$8
YInc:
	dc.w	$2
YSpeed:
	dc.w	$4
XSpeed:
	dc.w	$4

SpriteInfo:
	dc.l	Arrow1
	dc.l	LetterS
	dc.l	LetterT
	dc.l	LetterA
	dc.l	LetterM
	dc.l	LetterP
	dc.l	LetterE
	dc.l	LetterD
	dc.l	LetterE
	dc.l	Space
	dc.l	LetterD
	dc.l	LetterI
	dc.l	LetterS
	dc.l	LetterK
	dc.l	LetterB
	dc.l	LetterA
	dc.l	LetterS
	dc.l	LetterE
	dc.l	LetterD
	dc.l	Space
	dc.l	LetterM
	dc.l	LetterA
	dc.l	LetterG
	dc.l	LetterA
	dc.l	LetterZ
	dc.l	LetterI
	dc.l	LetterN
	dc.l	LetterE
	dc.l	Arrow2
	dc.l	Space

SpriteYPos:
	dc.l	SpriteYTable
SpriteYTable:
	incbin	C:\STAMPEDE.INC\1PLANEY.WAV
EndSpriteY:
	incbin	C:\STAMPEDE.INC\1PLANEY.WAV
	incbin	C:\STAMPEDE.INC\1PLANEY.WAV


SpriteXPos:
	dc.l	SpriteXTable+(4*125)
SpriteXTable:
	incbin	C:\STAMPEDE.INC\1PLANEX.WAV
EndSpriteX:
	incbin	C:\STAMPEDE.INC\1PLANEX.WAV
	incbin	C:\STAMPEDE.INC\1PLANEX.WAV

DelFreq	MACRO
	move.l	Last\1(PC),a0
	move.b	d0,(a0)
	move.b	d0,160(a0)
	move.b	d0,320(a0)
	move.b	d0,480(a0)
	move.b	d0,640(a0)
	move.b	d0,800(a0)
	move.b	d0,960(a0)
	move.b	d0,1120(a0)

	move.l	Current\1(PC),Last\1

	ENDM

FreqPlot	MACRO
FB\1
	moveq.l	#0,d0
	move.w	Player+\2(PC),d0
	tst.w	d0
	beq.s	.Skip\1
	lsr.w	#8,d0
	move.w	d0,d1
	lsr.w	#4,d0
	subq.w	#1,d0
	move.w	d0,Octave\1
	and.w	#$f,d1
	move.w	d1,Note\1
.Skip\1
	move.w	Octave\1(PC),d1
	ADD.W	D1,D1
	MOVE.W	D1,D0
	ADD.W	D1,D1
	ADD.W	D1,D0
	ADD.W	D0,D0
	
	add.w	Note\1(PC),d0
	lea	Offsets(PC),a0
	move.b	(a0,d0),d0

	move.l	ScreenPos(PC),a0
	lea	30720(a0),a0
	lea	6(a0,d0),a0
	
	ADD.W	#\3,A0
	move.l	a0,Current\1

	move.b	D2,(a0)
	move.b	D3,160(a0)
	move.b	D4,320(a0)
	move.b	D4,480(a0)
	move.b	D4,640(a0)
	move.b	D4,800(a0)
	move.b	D3,960(a0)
	move.b	D2,1120(a0)

	ENDM

DelVU:
	MOVEQ	#0,D0

	DelFreq	1
	DelFreq	2
	DelFreq	3
	DelFreq	4

	rts
VUDisplay:
	MOVE.B	#$3C,D2
	MOVE.B	#$7E,D3
	MOVE.B	#$FF,D4

	FreqPlot	1,$562,0
	FreqPlot	2,$564,1280
	FreqPlot	3,$566,1280*2
	FreqPlot	4,$568,1280*3

	rts

Note1:	dc.w	0
Note2:	dc.w	0
Note3:	dc.w	0
Note4:	dc.w	0
Octave1: dc.w	0
Octave2: dc.w	0
Octave3: dc.w	0
Octave4: dc.w	0
Last1:	dc.l	Space
Last2:	dc.l	Space
Last3:	dc.l	Space
Last4:	dc.l	Space
Current1: dc.l	Space
Current2: dc.l	Space
Current3: dc.l	Space
Current4: dc.l	Space

Offsets:
	dc.b	0,1,8,9,16,17
	dc.b	24,25,32,33,40,41
	dc.b	48,49,56,57,64,65
	dc.b	72,73,80,81,88,89
	dc.b	96,97,104,105
	dc.b	112,113,120,121
	dc.b	128,129,136,137
	dc.b	144,145,152,153
	dc.b	160,161

SECTION DATA

ScreenPos	dc.l	ScreenOne
ScreenPos2	dc.l	ScreenTwo	

SECTION BSS

*OldPalette	DS.W	16

TrueScreenPosition	DS.L	1

OldVBLVec	DS.L	1
OldHBLVec	DS.L	1
OldTimerA	ds.l	1

		ds.l	8*16
LetterS		ds.l	8*16
LetterT		ds.l	8*16
LetterA		ds.l	8*16
LetterM		ds.l	8*16
LetterP		ds.l	8*16
LetterE		ds.l	8*16
LetterD		ds.l	8*16
LetterI		ds.l	8*16
LetterK		ds.l	8*16
LetterB		ds.l	8*16
LetterG		ds.l	8*16
LetterZ		ds.l	8*16
LetterN		ds.l	8*16
Arrow2		ds.l	8*16
Arrow1		ds.l	8*16
Space		ds.l	8*16
		ds.l	128

Player:				*the replay routine
*	incbin	C:\STAMPEDE.INC\TRACKER.ROT
	ds.b	43700
	even
Module:					*the module
*	incbin 	C:\STAMPEDE.INC\MASQURAD.MOD
	ds.b	91000
	even

		ds.b	256+(5*1024)
		ds.b	16000
ScreenOne	ds.b	32000
		ds.b	16000
		ds.b	60*160

		ds.b	16000
ScreenTwo	ds.b	32000
		ds.b	16000
		ds.b	60*160
 